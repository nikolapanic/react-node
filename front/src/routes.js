const routes = {
    HOME: "/",
    login: "/login",
    register: "/register",
    logout: '/logout',
}

export default routes;